﻿<?php 
$start = microtime(true); // Start time 
$url = file('all.txt');
?>
<!DOCTYPE html>
<head>
<title></title>
  <link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>
</head>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<body>
    <div class="container">
      <br />
<table id="example" class="table table-striped table-bordered tablesorter" cellspacing="0" width="100%">
            <thead>
                <th>URL</th>
                <th>Server Responce</th>
                <th>Title</th>
                <th>Description</th>
                <th>Keywords</th>
                <th>Date</th>
            </thead>
            <tbody class="table-striped table-hover">   
<?php
function file_get_contents_curl($url)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}
foreach ($url as $value) {
	$value = trim($value);
	$responce = get_headers($value);
	$date = date("d-m-Y G:i:s");
	$html = file_get_contents_curl($value);

//parsing begins here:
$doc = new DOMDocument();
@$doc->loadHTML($html);
$nodes = $doc->getElementsByTagName('title');

//get and display what you need:
$title = $nodes->item(0)->nodeValue;

$metas = $doc->getElementsByTagName('meta');

for ($i = 0; $i < $metas->length; $i++)
{
    $meta = $metas->item($i);
    if($meta->getAttribute('name') == 'description')
        $description = $meta->getAttribute('content');
    if($meta->getAttribute('name') == 'keywords')
        $keywords = $meta->getAttribute('content');
}
?>
<tr>
    <td><a href="<?= $value ?>" target="_blank"><?= $value ?></a>
    <?php 
    if($responce[0] == "HTTP/1.1 301 Moved Permanently"){
        $color = "orange";
    } elseif ($responce[0] == "HTTP/1.1 200 OK") {
        $color = "green";
    } elseif ($responce[0] == "HTTP/1.1 404 File Not Found" || $responce[0] == "HTTP/1.1 500 Internal Server Error") {
        $color = "red";
    }
    ?>
    <td><strong><span style="color: <?= $color ?>"><?= $responce['0'] ?></span></strong></td>
    <td><?= $title ?></td>
    <td><?= $description ?></td>
    <td><?= $keywords ?></td>
    <td><?= $date ?></td>
</tr>
<?php
} // End foreach
?>
    </tbody>
</table>
<div class="align-rigth">
<?php echo '<strong><br />Время выполнения скрипта:</strong> '.(microtime(true) - $start).' сек.'; ?>
</div>
    </div>
</body>
</html>