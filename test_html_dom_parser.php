<?php 
	include_once 'simple_html_dom.php'; 
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
	<script language="javascript" src="/js/jquery-1.11.3.min.js" type="text/javascript"></script>
	<script language="javascript" src="/js/bootstrap.js" type="text/javascript"></script>
</head>
<body>
	<div class="container">
<table class="table table-hover table-condensed">
			<thead>
				<th>URL</th>
				<th>Server Responce</th>
				<th>Title</th>
				<th>Description</th>
				<th>Canonical</th>
				<th>Keywords</th>
				<th>Date</th>
			</thead>
			<tbody class="table-striped table-hover">	
<?php
$start = microtime(true); // Start time
$url = array(
			'http://tickets.az',
			'https://tickets.az/avia',
			'http://tickets.az/avia/pub/air/avia-faq',
			'http://tickets.az/avia/pub/air/avia-faq/passengers',
			'http://tickets.az/avia/pub/air/avia-faq/passengers/child-transportation.html',
			'https://tickets.az/avia/rating',
			'https://tickets.az/avia/rating/EK1',
			'https://tickets.az/avia/rating/EK/airline_reviews',
			'https://tickets.az/avia/rating/EK/punctuality',
			'http://tickets.az/avia/baku',
			'http://tickets.az/avia/kiev',
			'http://tickets.az/avia/direction/baku~kiev',
			'http://tickets.az/en',
			'https://tickets.az/en/avia',
			'http://tickets.az/en/avia/pub/air/avia-faq',
			'http://tickets.az/en/avia/pub/air/avia-faq/passengers',
			'http://tickets.az/en/avia/pub/air/avia-faq/passengers/child-transportation.html',
			'https://tickets.az/en/avia/rating',
			'https://tickets.az/en/avia/rating/EK',
			'https://tickets.az/en/avia/rating/EK/airline_reviews',
			'https://tickets.az/en/avia/rating/EK/punctuality',
			'http://tickets.az/en/avia/baku',
			'http://tickets.az/en/avia/kiev',
			'http://tickets.az/en/avia/direction/baku~kiev'
);

foreach ($url as $value) {
	$response =	get_headers($value);
	$date = date("d-m-Y G:i:s");
	if ($response[0] == "HTTP/1.1 404 File Not Found") { ?>
		<tr>
			<td><?= $value ?></td>
			<td bgcolor="#F5A9A9"> <?= $response[0] ?></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td><?= $date ?></td>
		</tr>
	<?php } else {
					$html = file_get_html($value);
					$canonical = $html->find('head link[rel=canonical]', 0)->href;
					$title = $html->find('head title', 0)->plaintext;
					$description = $html->find('head meta[name=description]', 0)->content;
					$keywords = $html->find('head meta[name=keywords]', 0)->content; 
	?>
			<td><?= $value ?></td>
			<td><?= $response[0] ?></td>
			<td><?= $title ?></td>
			<td><?= $description ?></td>
			<td><?= $canonical ?></td>
			<td><?= $keywords ?></td>
			<td><?= $date ?></td>
		</tr>					
				<?php 
			} 
		} 
	?>
			</tbody>
		</table>
<div class="align-rigth">
<?php echo '<strong><br />Время выполнения скрипта:</strong> '.(microtime(true) - $start).' сек.'; ?>
</div>
	</div>
</body>
</html>
