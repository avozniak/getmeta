<?php
include_once 'simple_html_dom.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
	<script language="javascript" src="/js/jquery-1.11.3.min.js" type="text/javascript"></script>
	<script language="javascript" src="/js/bootstrap.js" type="text/javascript"></script>
</head>
<body>
	<div class="container">
<table class="table table-hover table-condensed">
			<thead>
				<th>URL</th>
				<th>Server Responce</th>
				<th>Title</th>
				<th>Description</th>
				<th>Canonical</th>
				<th>Keywords</th>
				<th>Date</th>
			</thead>
			<tbody class="table-striped table-hover">	
			<?php
			// $link = mysqli_connect('localhost', 'root', '', 'meta');
			// if (!$link) {
   // 			 die('Ошибка подключения (' . mysqli_connect_errno() . ') '
   //          . mysqli_connect_error());
			// }
			// 	echo 'Соединение установлено... ' . mysqli_get_host_info($link) . "\n";
				foreach ($url as $value) {
					$response =	get_headers($value);
					// $html = file_get_html($value);
					// $canonical = $html->find('head link[rel=canonical]', 0)->href;
					$date = date("d-m-Y G:i:s");
					$meta = get_meta_tags($value);
					$kw = $meta['keywords'];
					$desc = $meta['description'];
					$str = file_get_contents($value);
					preg_match('/<title>(.*)<\/title>/s', $str, $title);
					// $sql = "INSERT INTO TicketsUa (url, response, title, description, keywords, canonical, date) VALUES ('$value', '$response[0]', '$title[1]', '$desc', '$kw', '$canonical', NOW())";
					// $result = mysqli_query($link, $sql) or die(mysqli_error($link));
			?>
			<tr>
				<td><?= $value ?></td>
				<?php
					if ($response[0] !== "HTTP/1.1 200 OK") {
						?> <td bgcolor="#F5A9A9"> <?= $response[0] ?></td>
				<?php	} else {?>
					<td> <?= $response[0] ?></td>
				<?php } ?> 

				<td><p><?= $title[1] ?></p></td>
				<td><?= $desc ?></td>
				<td><?= $canonical ?></td>
				<td><?= $kw ?></td>
				<td><?= $date ?></td>
			</tr>
		
		<?php 

 		}  // Закінчення foreach ($url as $value)
 			// mysqli_close($link);
		?>
 			</tbody>
			</table>
		<div class="align-rigth">
		<?php echo '<strong><br />Время выполнения скрипта:</strong> '.(microtime(true) - $start).' сек.';	?>
		</div>
		</div>
</body>
</html>
